
export default class Storage {

  constructor(prefix) {
    this.prefix = prefix;
  }

  get(key, fallback=null) {
    const value = localStorage.getItem(`${this.prefix}/${key}`);
    return value ? value : fallback;
  }

  set(key, value) {
    localStorage.setItem(`${this.prefix}/${key}`, value);
  }

  remove(key) {
    localStorage.removeItem(`${this.prefix}/${key}`);
  }


  dump(state) {
    // write every property of state in cache
    for (const key in state) {
      this.set(key, state[key]);
    }
  }

  load(state) {
    // update every property of state from cache
    for (const key in state) {
      const value = this.get(key);
      if (value != null) state[key] = value;
    }
  }

}
