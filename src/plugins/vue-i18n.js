import Vue from 'vue';
import VueI18n from 'vue-i18n';
import {en_US} from '../i18n/en_US';
import {fr_FR} from '../i18n/fr_FR';

Vue.use(VueI18n);

export const messages = {
  en_US: en_US,
  fr_FR: fr_FR,
};

export default new VueI18n({
  locale: 'fr_FR',
  fallbackLocale: 'en_US',
  messages
});

