This folder contains end-to-end tests.
Tests use link:https://www.selenium.dev/[Selenium] and link:https://nightwatchjs.org/[Nightwatch.js].

Configuration is done using `vue-cli`.
See link:https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-e2e-nightwatch[`cli-plugin-e2e-nightwatch` documentation] for more information.

Launch options can be seen/modified in `package.json`, at the root of the project.
