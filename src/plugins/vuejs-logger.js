import Vue from 'vue';
import Logger from 'vuejs-logger';

export function initialize(isProduction) {
  const config = {
    isEnabled: true,
    logLevel : isProduction ? 'error' : 'debug',
    stringifyArguments : false,
    showLogLevel : true,
    showMethodName : !isProduction,
    separator: '|',
    showConsoleColors: true,
  };
  Vue.use(Logger, config);
}

