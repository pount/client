export const fr_FR = {
  menu: {
    home: "Page d'accueil",
    about: "À propos",
    help: "Aide",
    settings: "Paramètres",
    dark: "Mode Nuit : {mode}",
    feedback: "Contactez-nous !",
    language: "Langue : {current}",
    user: {
      profile: "Mon profil",
      auth: {
        login: "Me connecter",
        logout: "Me déconnecter",
        description: "Connectez-vous pour visualiser vos propres données sur Pount.",
        creation: "Lors de votre première connexion, un compte utilisateur sera créé.",
        cookies: "Pour vous connecter avec Google ou GitHub, les cookies tiers doivent être autorisés dans les préférences de votre navigateur.",
        more: "En savoir plus",
        method: {
          shibboleth: "Continuer avec Shibboleth",
          google: "Continuer avec Google",
          github: "Continuer avec GitHub",
          facebook: "Continuer avec Facebook",
          twitter: "Continuer avec Twitter",
        },
      },
    },
  },
  set: {
    title: "Titre",
    description: "Description",
    template: "Modèle",
    visibility: "Visibilité",
  },
  cancel: "Annuler",
  on: "ON",
  off: "OFF",
  public: "Publique",
  private: "Privée",
  language: {
    en_US: "English",
    fr_FR: "Français",
  },
};
