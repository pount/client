export const en_US = {
  menu: {
    home: "Home",
    about: "About",
    help: "Help",
    settings: "Settings",
    dark: "Dark Theme: {mode}",
    feedback: "Contact us!",
    language: "Language : {current}",
    user: {
      profile: "My profile",
      auth: {
        login: "Sign in",
        logout: "Sign out",
        description: "Sign in to visualize your own data with Pount.",
        creation: "If it is your first connection, a user account will be created.",
        cookies: "To sign in with Google or GitHub, third-party cookies must be authorized in your browser settings.",
        more: "More",
        method: {
          shibboleth: "Continue with Shibboleth",
          google: "Continue with Google",
          github: "Continue with GitHub",
          facebook: "Continue with Facebook",
          twitter: "Continue with Twitter",
        },
      },
    },
  },
  set: {
    title: "Title",
    description: "Description",
    template: "Template",
    visibility: "Visibility level",
  },
  cancel: "Cancel",
  on: "ON",
  off: "OFF",
  public: "Public",
  private: "Private",
  language: {
    en_US: "English",
    fr_FR: "Français",
  },
};
