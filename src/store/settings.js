import Vue from 'vue';
import { config } from '../plugins/vuetify';
import i18n from '@/plugins/vue-i18n';
import Storage from './storage';

const storage = new Storage('pount/settings');

const CALLBACKS = {
  dark : ({ value, vuetify }) => {
    if (vuetify) vuetify.theme.dark = value;
  },
  locale: ({ value }) => {
    i18n.locale = value;
  },
  remember: ({ value, state }) => {
    if (value) {
      // If user allows it, store all her settings in cache
      storage.dump(state);
    } else {
      // If user doesn't want to be tracked, forget everything we knew,
      // and then store her choice so we don't track everything next time
      for (const key in state) storage.remove(key);
      storage.set('remember', value);
    }
  },
};


export const Settings = {
  namespaced: true,
  state: {
    remember: true,
    dark: config.theme.dark,
    locale: i18n.locale,
  },
  getters: {
    dark: state => { return storage.get('dark', state.dark); },
    locale: state => { return storage.get('locale', state.locale); },
    initFromCache(state) {
      // This method is not exactly intended to be used as a vuex getter
      // instead, it just exists to be called from main.js to initialize
      // our state with values stored in cache, if there are any.
      let remember = storage.get('remember');
      if (remember == null) {
        remember = state.remember;
        if (remember) storage.dump(state);
      } else {
        remember = (remember === 'true');  // string to boolean
      }
      if (remember) storage.load(state);
      // To save resources, we'll from now on only use values from state
      // and won't read from cache anymore. Next time we'll write to cache
      // will only be when user changes her settings.
      return remember;
    },
  },
  mutations: {
    update(state, { key, value }) {
      if (key in state) {
        Vue.set(state, key, value);
        if (state.remember) storage.set(key, value);
      } else {
        Vue.$log.error(`'${key}' not found in settings.`);
      }
    },
  },
  actions: {
    update({ commit }, payload) {
      commit('update', { key: payload.key, value: payload.value });
      const callback = CALLBACKS[payload.key];
      if (callback) callback(payload);
    },
  },
}
