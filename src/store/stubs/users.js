import Vue from 'vue';

const axios = require('axios').default;
const baseurl = 'https://swapi.dev/api/people/';



export class User {
  constructor() {
    this.id = null;
    this.name = null;
    this.email = null;
  }

  update(payload) {
    for (let property in this) {
      if (property == 'id') continue;  // never change a User unique id
      if (Object.prototype.hasOwnProperty.call(payload, property)) {
        this[property] = payload[property];
      }
    }
  }

  static create(payload) {
    let user = new User();
    user.id = payload.id;
    user.name = payload.name;
    user.email = `${payload.gender}@${payload.skin_color}.${payload.hair_color}`
    return user;
  }
}

export const Users = {
  namespaced: true,
  state: {
    users: new Map(),
  },
  getters: {
    uids: state => { return state.users.keys(); },
    users: state => { return state.users.values(); },
    usernames: state => { return state.array.map(user => user.name); },
  },
  mutations: {
    add(state, user) {
      // mutations should be as simple as possible, so we don't check anything
      // actions are where to throw errors which are to be caught in component
      state.users.set(user.id, user);
    },
    update(state, payload) {
      let old = state.users.get(payload.id);
      old.update(payload);
    },
  },
  actions: {
    get({ commit, getters }, page) {  // eslint-disable-line no-unused-vars
      return axios.get(`${baseurl}?page=${page}`)
        .then(function (response) {
          Vue.$log.debug('axios/get:', response.data);
          return response.data;
        })
        .catch(function (error) {
          Vue.$log.error('axios/get:', error);
          return { count: 0, previous: null, next: null, results: [] };
        })
    },
  }
}
