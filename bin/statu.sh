#!/bin/bash

# see: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
last_commit=$CI_COMMIT_SHA
branch=$CI_COMMIT_BRANCH
pipeline=$CI_PIPELINE_ID
date=`date`
commits=`git rev-list --all --count`

echo -e "{
  \"sha\":\"$last_commit\",
  \"branch\":\"$branch\",
  \"pipeline\":\"$pipeline\",
  \"date\":\"$date\",
  \"commits\":\"$commits\"
}"
