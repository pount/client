import Vue from 'vue';
import Storage from './storage';

const storage = new Storage('pount/app');

export const App = {
  namespaced: true,
  state: {
    showAuthDialog: false,
    authInProgress: false,
  },
  getters: {
    showAuthDialog: state => { return state.showAuthDialog; },
    authInProgress: state => { return storage.get('authInProgress', state.authInProgress); },
  },
  mutations: {
    showAuthDialog(state, value) {
      Vue.set(state, 'showAuthDialog', value);
    },
    authInProgress(state, value) {
      Vue.set(state, 'authInProgress', value);
      if (value) storage.set('authInProgress', value);
      else storage.remove('authInProgress');
    },
  },
  actions: {
    showAuthDialog({ commit }, value) {
      commit('showAuthDialog', value);
    },
    authInProgress({ commit }, value) {
      commit('authInProgress', value);
    },
  },
}
