Pount: Scientific data under the spotlight!
===========================================

.. |pipeline| image:: https://gitlab.com/pount/client/badges/master/pipeline.svg
    :target: https://gitlab.com/pount/client/commits

.. |coverage| image:: https://gitlab.com/pount/client/badges/master/coverage.svg
    :target: https://gitlab.com/pount/client/pipelines/latest

.. |license| image:: https://img.shields.io/badge/license-AGPL3.0-informational?logo=gnu&color=important
    :target: https://www.gnu.org/licenses/agpl-3.0.html

.. |version| image:: https://img.shields.io/badge/dynamic/json.svg?label=updated&logo=vuetify&query=date&url=https%3A%2F%2Fpount.gitlab.io%2Fclient%2Fversion.json
    :target: https://pount.gitlab.io/client/

| |license|
| |pipeline| |coverage|
| |version|

*Pount* is an elegant, simple and free software to help you structure, visualize and valorize your data.

Please check project documentation available on `Read the Docs <https://pount.readthedocs.io/en/latest/>`_.


