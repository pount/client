import { expect } from 'chai'
import { createLocalVue, config, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'
import App from '@/app.vue'
import About from '@/views/about.vue'

const localVue = createLocalVue()
localVue.use(Vuex);
// mock vue-i18n translation method so it returns the key as is
config.mocks.$t = key => key;

describe('About', () => {
  it('renders "about" text', () => {
    const wrapper = shallowMount(About)
    expect(wrapper.text()).to.include('about')
  })
})

describe('Homepage', () => {

  it('renders application title', () => {
    const store = new Vuex.Store({
      modules: {
        user: {
          namespaced: true,
          getters: {
            isSignedIn: state => { return false; },  // eslint-disable-line no-unused-vars
          },
        },
        settings: {
          namespaced: true,
          getters: {
            dark: state => { return false; },  // eslint-disable-line no-unused-vars
          },
        }
      }
    })
    const wrapper = shallowMount(App, {
      localVue,
      store,
      // without this kind of precaution, tests will fail because it won't
      // know these 2 custom elements: <router link/> and <router-view/>.
      // see: https://vue-test-utils.vuejs.org/guides/using-with-vue-router.html
      stubs: ['router-link', 'router-view', ],
    })
    expect(process.env.VUE_APP_NAME).to.equal('pount')
    expect(wrapper.text()).to.include(process.env.VUE_APP_NAME)
  })
})
