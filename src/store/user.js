import Vue from 'vue'



const axios = require('axios').default;

axios.interceptors.request.use(
  async config => {
    const token = User.state.token;
    config.headers.Authorization = `FirebaseToken ${token}`;
    return config;
  },
  error => Promise.reject(error),
)



export const User = {
  namespaced: true,
  state: {
    name: null,
    token: null,
  },
  getters: {
    name: state => { return state.name; },
    uid: state => { return state.uid; },
    info: state => { return {
        name: state.name,
        email: state.email,
        phone: state.phone,
        avatar: state.avatar,
      }
    },
    isSignedIn: state => { return state.token != null; }
  },
  mutations: {
    signIn(state, user) {
      // only update what info we are given and leave the rest as is
      if (user.token) Vue.set(state, 'token', user.token);
      if (user.uid) Vue.set(state, 'uid', user.uid);
      if (user.name) Vue.set(state, 'name', user.name);
      if (user.email) Vue.set(state, 'email', user.email);
      if (user.phone) Vue.set(state, 'phone', user.phone);
      if (user.avatar) Vue.set(state, 'avatar', user.avatar);
    },
    signOut(state) {  // forget all the things!
      for (const key in state) state[key] = null;
    },
  },
  actions: {
    update({ commit }, user) {
      commit('signIn', user);
    },
    signOut({ commit }) {
      commit('signOut');
    },
  }
}
