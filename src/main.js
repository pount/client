import Vue from 'vue'
import App from './app.vue'
import router from './router'
import store from './store'
import './store/storage'
import vuetify from './plugins/vuetify'
import i18n from './plugins/vue-i18n'

const isProduction = process.env.NODE_ENV === 'production';
Vue.config.productionTip = isProduction;

// use a true logger, because console.log(...) only goes so far
import {initialize} from './plugins/vuejs-logger'
initialize(isProduction);

// use firebase authentication
import firebase from 'firebase/app'
let firebaseConfig = {
  };
firebase.initializeApp(firebaseConfig);

// makes rich text editor available in application forms
import { TiptapVuetifyPlugin } from 'tiptap-vuetify'
Vue.use(TiptapVuetifyPlugin, { vuetify, iconsGroup: 'mdi' })

store.getters['settings/initFromCache'];
i18n.locale = store.getters['settings/locale'];

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),  // https://www.w3schools.com/js/js_arrow_function.asp
}).$mount('#app')
