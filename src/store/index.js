import Vue from 'vue'
import Vuex from 'vuex'
import {Users} from './stubs/users'
import {User} from './user'
import {Sets} from './demo/sets'
import {App} from './app'
import {Settings} from './settings'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    app: App,
    settings: Settings,
    users: Users,
    user: User,
    sets: Sets,
  }
});
