import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/home.vue'
import Settings from '../views/settings.vue'
import Set from '@/views/sets/render.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/about.vue'),
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings,
    meta: {
      requiresAuthentication: true,
    },
  },
  {
    path: '/login',
    name: 'signin',
    component: () => import(/* webpackChunkName: "signin" */ '../views/signin.vue'),
  },
  {
    path: '/sets/create',
    name: 'set.create',
    props: true,
    component: () => import(/* webpackChunkName: "edit" */ '../views/sets/update.vue'),
    meta: {
      requiresAuthentication: true,
    },
  },
  {
    path: '/sets/:id',
    name: 'set',
    props: true,
    component: Set,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  const shielded = to.matched.some((route) => {
      return route.meta.requiresAuthentication;
  });
  const isSignedIn = store.getters['user/isSignedIn'];
  Vue.$log.debug(
      from.name, '[', from.path, ']',
      ' --(', shielded, ',', isSignedIn, ')--> ',
      to.name, '[', to.path, ']'
  );
  if (shielded && !isSignedIn ) {
    next({ name: 'signin', query: { redirect: to.name } });
  } else {
    next();
  }
});

export default router;
