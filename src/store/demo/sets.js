import Vue from 'vue';


export const Sets = {
  namespaced: true,
  state: {
    templates: {
      'minimal': {
        'title': {
          type: 'str',
          mandatory: true,
          value: null,
        },
      },
      'datacite': {
        'identifier': {
          type: 'str',
          mandatory: true,
        },
        'creator': {
          type: 'str',
          mandatory: true,
        },
        'title': {
          type: 'str',
          mandatory: true,
        },
        'publisher': {
          type: 'str',
          mandatory: true,
        },
        'publicationYear': {
          type: 'int',
          mandatory: true,
        },
        'subject': {
          type: 'str',
          recommended: true,
        },
        'contributor': {
          type: 'str',
          recommended: true,
        },
        'date': {
          type: 'date',
          recommended: true,
        },
        'language': {
          type: 'str',
        },
        'resourceType': {
          type: 'str',
          mandatory: true,
        },
        'alternateIdentifier': {
          type: 'str',
        },
        'relatedIdentifier': {
          type: 'str',
          recommended: true,
        },
        'size': {
          type: 'str',
        },
        'version': {
          type: 'str',
        },
        'rights': {
          type: 'str',
        },
        'description': {
          type: 'str',
          recommended: true,
        },
        'geolocation': {
          type: 'str',
          recommended: true,
        },
        'fundingReference': {
          type: 'str',
        },
      },
    },
    sets: [
      {
        id: '1',
        metadata: {
          title: 'Default set',
          description: 'Set already created for testing purposes',
        },
        settings: {
          template: 'minimal',
          public: true,
        },
      },
    ],
  },
  getters: {
    set: state => id => { return state.sets.find(s => s.id === id); },
    template: state => id => { return state.templates[id]; },
  },
  mutations: {
    create(state, payload) {
      state.sets.push(payload);
    }
  },
  actions: {

    get({ getters }, id) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          let set = getters.set(id);
          Vue.$log.debug(`GET /sets/${id}/ :`, set);
          if (set) resolve(set);
          else reject();
        }, 1000)
      });
    },

    create({ commit, state }, payload) {
      return new Promise((resolve) => {
        setTimeout(() => {
          let id = String(state.sets.length + 1);
          payload['id'] = id;
          commit('create', payload);
          Vue.$log.info(`POST /sets/ : ${id}`);
          resolve(id);
        }, 1000)
      });
    },

    templates({ state }) {
      return new Promise((resolve) => {
        setTimeout(() => {
          let templates = Object.keys(state.templates);
          Vue.$log.debug(`GET /templates/ :`, templates);
          resolve(templates);
        }, 1000)
      });
    },


  }
}
